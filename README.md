# weather-sky

App developed By Edward S. Ramos

Simple and easy for get the weather information about your city.

## Project setup
  - Clone the repo
  - Be sure to be located on package.json at root:

  ```
  npm run install-lib
  ```

## Run Project
  - Please ask for the `.env` file and paste it inside `api` folder - or can create a new one based on `.env-sample`-
  - Be sure to be located on package.json at root:
  - Run `start-api` for start the node API server
  - Run `start-web` for start the web server in new terminal.
  - If everything going good you should be able to see the home page:
