const axios = require('axios')
const { endpoints } = require('../config')

const apiClient = (baseUrl) => {
  return axios.create({
    baseURL: baseUrl,
    withCredentials: false,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    timeout: 20000
  })
}

const getCoordsBySearch = (place) => {
  return apiClient(endpoints.mapbox).get(`${place}.json?access_token=${process.env.TOKEN_MAPBOX}`)
}

const getWeatherByPlace = (coords, simple) => {
  const blocks = simple ? '?exclude=minutely,hourly,daily,alerts,flags' : '?exclude=currently,flags'

  return apiClient(endpoints.darksky).get(`/${coords}/${blocks}&units=ca`)
}

const getWeatherByPlaceTime = (coords, time) => {
  return apiClient(endpoints.darksky).get(`/${coords},${time}?exclude=flags&units=ca`)
}

module.exports = {
  getCoordsBySearch,
  getWeatherByPlace,
  getWeatherByPlaceTime
}
