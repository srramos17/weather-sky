module.exports = {
  endpoints: {
    darksky: `https://api.darksky.net/forecast/${process.env.TOKEN_DARK}`,
    mapbox: 'https://api.mapbox.com/geocoding/v5/mapbox.places'
  }
}