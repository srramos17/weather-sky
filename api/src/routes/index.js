const { Router } = require('express')
const { getCoordsBySearch, getWeatherByPlace, getWeatherByPlaceTime } = require('../services')
const router = Router()

router.get('/place/coords', async (req, res) => {
  let response = {}

  try {
    const { search } = req.query
    response = await getCoordsBySearch(search)
  } catch (err) {
    return res.status(400).send(err)
  }

  res.status(200).send(response.data)
})

router.get('/place/weather', async (req, res) => {
  let response = {}

  try {
    const { coords, simple } = req.query
    response = await getWeatherByPlace(coords, simple)
  } catch (err) {
    return res.status(400).send(err)
  }

  res.status(200).send(response.data)
})

router.get('/place/time', async (req, res) => {
  let response = {}

  try {
    const { coords, time } = req.query

    response = await getWeatherByPlaceTime(coords, time)
  } catch (err) {
    return res.status(400).send(err)
  }

  res.status(200).send(response.data)
})

module.exports = router
