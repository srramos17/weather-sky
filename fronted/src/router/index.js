import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/place/',
    name: 'place',
    props: true,
    component: () => import('@/views/Place.vue'),
    beforeEnter (routeTo, routeFrom, next) {
      if (!routeTo.params.place) {
        next({ name: 'home' })
      }

      next()
    }
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/NotFound404.vue')
  },
  {
    path: '*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  routes
})

export default router
