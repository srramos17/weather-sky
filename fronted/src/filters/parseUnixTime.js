const parseUnixTime = (time) => {
  if (!time) {
    return
  }

  const months = ['Jan', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  const date = new Date(time * 1000)

  return `${months[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`
}

export { parseUnixTime }
