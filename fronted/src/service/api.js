import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://localhost:3000',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 20000
})

const getPlaceCoords = (search) => {
  return apiClient.get('/place/coords', { params: { search } })
}

const getPlaceData = (coords, simple) => {
  return apiClient.get('/place/weather', { params: { coords: coords.join(','), simple } })
}

const getDataByTime = (coords, time) => {
  return apiClient.get('/place/time', { params: { coords: coords.join(','), time } })
}

export {
  getPlaceCoords,
  getPlaceData,
  getDataByTime
}
